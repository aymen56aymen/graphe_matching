#include <iostream>
#include <cassert>
#include <fstream>
#include "Edmonds.hpp"

Graph::~Graph()
{
	for (auto i = 0; i < VerticesNbr; i++)
	{
  		delete [] GraphEdges[i];
	}
	delete GraphEdges;
	delete Vertices_State;
}

Graph::Graph(const std::string GraphPath)
{
	std::ifstream file(GraphPath);
	file >> VerticesNbr;
	ExposedVetricesNbr = VerticesNbr;
	GraphEdges = new int*[VerticesNbr];
	Vertices_State = new int[VerticesNbr];
	assert (GraphEdges && Vertices_State);
	for (auto i = 0; i < VerticesNbr; i++)
	{
		Vertices_State[i] = 0; // Déclarer le sommet numéro i exposé
		int Edges_Nbr = 0;
  		GraphEdges[i] = new int[VerticesNbr];
		assert (GraphEdges[i]);
		for (auto j = 0; j < VerticesNbr; j++)
		{
			file >> GraphEdges[i][j];
			if (GraphEdges[i][j] != 0)
			{
				Edges_Nbr++;
				if (GraphEdges[i][j] % 3 == 0)
				{
					Vertices_State[i] = j+1; // Le sommet "i" n'est plus exposé parce que l'arête qui le lie au sommet "j" fait partie du couplage
					ExposedVetricesNbr--;
				}
			}
		}
		GraphEdges[i][i] = Edges_Nbr;
	}
	
}

std::size_t Graph::GraphSize() const
{
	return VerticesNbr;
}

std::size_t Graph::GetExpNbr() const
{
	return ExposedVetricesNbr;
}

int& Graph::VerticesState(const std::size_t indice)
{
	assert ( indice > 0 && indice <= VerticesNbr);
	return Vertices_State[indice-1];
}

int& Graph::operator()(const std::size_t indice_i, const std::size_t indice_j)
{
	assert ( indice_i > 0 && indice_i <= VerticesNbr && indice_j > 0 && indice_j <= VerticesNbr );
	return GraphEdges[indice_i-1][indice_j-1];
}

const int& Graph::operator()(const std::size_t indice_i, const std::size_t indice_j) const
{
	assert ( indice_i > 0 && indice_i <= VerticesNbr && indice_j > 0 && indice_j <= VerticesNbr );
	return GraphEdges[indice_i-1][indice_j-1];
}

void Graph::IncreaseMatching(const int& indice_i, const int& indice_j)
{
	assert ( indice_i > 0 && indice_i <= VerticesNbr && indice_j > 0 && indice_j <= VerticesNbr && GraphEdges[ indice_j-1 ][ indice_i-1 ] % 3 != 0 ); // S'assurer que les sommets ne sont pas couplés
	GraphEdges[ indice_i-1 ][ indice_j-1 ] += 2;
	GraphEdges[ indice_j-1 ][ indice_i-1 ] += 2;
	VerticesState( indice_i ) = indice_j;
	VerticesState( indice_j ) = indice_i;
	
}

void Graph::DecreaseMatching(const int& indice_i)
{
	int indice_j = VerticesState(indice_i);
	assert (indice_i > 0 && indice_i <= VerticesNbr && indice_j != 0 && GraphEdges[ indice_i-1 ][ indice_j-1 ] % 3 == 0); // S'assurer que le sommet indice_i est couplé
	GraphEdges[ indice_i-1 ][ indice_j-1 ] -= 2;
	GraphEdges[ indice_j-1 ][ indice_i-1 ] -= 2;
	VerticesState( indice_j ) = 0;
	VerticesState( indice_i ) = 0;
	
}

Vertex::Vertex(const Graph& graph, const std::size_t i) // i doit etre >= 1
{
	EdgesNbr = graph(i,i);
	int ind = 0;
	Index = i;
	VertexTree[0] = 0;
	VertexTree[1] = 0;
	VertexTree[2] = 0;
	Father = NULL;

	for (auto j = 0; j < 4; j++)
	{
		VertexEdges[j] = new int[graph(i,i)];
		assert ( VertexEdges[j] );
	}

	for (auto j = 1; j <= graph.GraphSize(); j++)
	{
		if ( graph(i,j) != 0 && i != j)
		{
			VertexEdges[0][ind] = j;
			VertexEdges[1][ind] = 0;
			VertexEdges[2][ind] = 0;
			VertexEdges[3][ind] = 0;
			ind++;
		}
	}
}

Vertex::~Vertex()
{
	for (auto j = 0; j < 4; j++)
	{
		delete [] VertexEdges[j];
	}
}

std::size_t Vertex::VertexIndex()
{
	return Index;
}

int Vertex::Root()
{
	return VertexTree[0];
}

int Vertex::DistanceToRoot()
{
	return VertexTree[1];
}

void Vertex::AddSon(Vertex& V)
{
	for (auto i = 0; i < EdgesNbr; i++)
	{
		if (VertexEdges[0][i] == V.VertexIndex())
		{
			VertexEdges[2][i] = 2; // V est un fils du sommet courant
			break;
		}
	}
}

void Vertex::AddFather(Vertex& V)
{
	Father = &V;
	for (auto i = 0; i < EdgesNbr; i++)
	{
		if (VertexEdges[0][i] == V.VertexIndex())
		{
			VertexEdges[2][i] = 1; // V est un père du sommet courant
			break;
		}
	}
	V.AddSon((*this));
}

void Vertex::AddTotree(Vertex& V) // On donne en argument le père
{
	if (V.VertexIndex() == Index) // Ca veut dire qu'on déclare un nouveau arbre
	{
		VertexTree[0] = Index;
		VertexTree[1] = 0; // outer
		VertexTree[2] = 0; // non marqué dans l'arbre
	}
	else // Pour ajouter le sommet à un arbre déja déclarer
	{
		VertexTree[0] = V.Root();
		if (V.DistanceToRoot() == 0)
		{
			VertexTree[1] = 1; // Inner
			VertexTree[2] = 1; // Une sommet inner est marqué par défaut
		}
		else  
		{
			VertexTree[1] = 0; // Outer
			V.UnMarkedDirect(*this);
			MarkEdge(V.VertexIndex()); // L'arete qui lie un sommet outer avec son sommet père est marquée par défaut
		} 
		this->AddFather(V);
	}
}


Vertex& Vertex::GetFather() // Pas encore utilisée
{
	return *Father;
}

void Vertex::MarkVertex()
{
	VertexTree[2] = 1;
	Father->MarkedDirect(*this);
}

void Vertex::UnMarkedDirect(Vertex& V) // La direction de l'arete V à partir du sommet courant, contient un sommet non marqué, le synthaxe est donc (père).UnMarkedDirect(fils)
{
	bool verif = true;
	for (auto i = 0; i < EdgesNbr; i++)
	{
		if (VertexEdges[3][i] == 1 && verif)
		{
			verif = false;
		}
		if (VertexEdges[0][i] == V.VertexIndex())
		{
			VertexEdges[3][i] = 1;
		}
	}
	if (verif && Index != VertexTree[0]) // Si c la 1er fois que l'un des fils du sommet courant (qui est différent du root) reporte une direction non marquée, le sommet courant report de son coté une direction non marquée pour son père.  
	{
		Father->UnMarkedDirect(*this);
	}
}

void Vertex::MarkedDirect(Vertex& V) // La direction de l'arete V à partir du sommet courant, ne contient plus un sommet non marqué, le synthaxe est donc (père).MarkedDirect(fils)
{
	bool verif = true;
	for (auto i = 0; i < EdgesNbr; i++)
	{
		if (VertexEdges[0][i] == V.VertexIndex())
		{
			VertexEdges[3][i] = 0;
		}
		if (VertexEdges[3][i] == 1 && verif)
		{
			verif = false;
		}
	}
	if (verif && Index != VertexTree[0]) 
	{
		Father->MarkedDirect(*this);
	}
}

int Vertex::GetUnMarkedEdge()
{
	int result = 0;
	for (auto i = 0; i < EdgesNbr; i++)
	{
		if (VertexEdges[1][i] == 0 && result == 0)
		{
			result = VertexEdges[0][i];
		}
	}
	return result;
}

void Vertex::MarkEdge(int indice)
{
	for (auto i = 0; i < EdgesNbr; i++)
	{
		if (VertexEdges[0][i] == indice)
		{
			VertexEdges[1][i] = 1;
			break;
		}
	}
}








