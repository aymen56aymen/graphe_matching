#ifndef DEF_Edmonds
#define DEF_Edmonds

class Graph {
  public :
  	Graph(const std::string GraphPath);
  	~Graph();
	std::size_t GraphSize() const;
	std::size_t GetExpNbr() const;
	int& operator()(const std::size_t, const std::size_t);
	const int& operator()(const std::size_t, const std::size_t) const;
	int& VerticesState(const std::size_t);
	void IncreaseMatching(const int& , const int&); // Cette fonction permet d'ajouter une arete au couplage
	void DecreaseMatching(const int&);  // Cette fonction permet d'enlever une arete au couplage 
  private :
	std::size_t VerticesNbr;  	
	int** GraphEdges;  // la case GraphEdges(i,i) a pour but de stocké le nombre d'arête du sommet "i"
	int* Vertices_State; // Ce tableau permet de stocker l'état des sommets (couplé "l'indice du sommet avec lequelle il est couplé" ou non "0").
	std::size_t ExposedVetricesNbr;
	//Il est aussi possible de stocker le nbr des sommets exposés
};

class Vertex {
  public :
	Vertex(const Graph&, const std::size_t);
	~Vertex();
	std::size_t VertexIndex();
	int Root();
	int DistanceToRoot();
	void AddTotree(Vertex&); // On donne le sommet père en argument
	void AddSon(Vertex&);  // Cette fonction est appelée dans la fonction AddFather
	void AddFather(Vertex&); // Cette fonction est appelée dans la fonction AddTotree
	void MarkVertex();
	void UnMarkedDirect(Vertex&);
	void MarkedDirect(Vertex&);
	int GetUnMarkedEdge();
	void MarkEdge(int);
	Vertex& GetFather();
  private :
	int* VertexEdges[4]; // la 1er ligne contient les indices des sommet lié à "Vertex" par des aretes. la 2eme ligne permet de savoir si l'arete est marqué ou non. la 3eme si le sommet ne fait pas partie du meme arbre "0", si oui, il est soit père "1" soit fils "2". Et la 4ème si il y'a un sommet non marker par cette direction.
	int EdgesNbr; // le nombre des aretes du sommet courant
	std::size_t Index; // >= 1
	int VertexTree[3]; // Si le sommet appartient à un arbre, la 1er case de ce tableau va contenir son origine (root) et la 2eme si le sommet est à une distance pair (outer) "0" ou impaire (inner) "1" de cette origine. La 3eme case permet de savoir si le sommet est marqué ou non.
	Vertex* Father;

};

/*class Forest {
  public :
	Forest(

}*/

#endif
